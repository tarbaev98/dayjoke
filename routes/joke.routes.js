const express = require('express')
const router = express.Router()
const Jokes = require('../shema/shema.joke')

router.route('/joke').get((req, res) => {
    Jokes.countDocuments().exec((err, count) => {
        if (err) console.log(err)
        let rand = Math.floor(Math.random() * count)
        Jokes.findOne().skip(rand).exec((err1, result) => {
            if (err1) console.log(err1)
            res.status(200).json(result)
        })
    })
})

router.route('/joke/:id').put((req, res) => {
    Jokes.findOneAndUpdate({id: req.params.id}, {like: req.body.like + 1}, {new: true}, (err, doc) => {
        if (err) console.log(err)
        res.status(201).json(doc)
    })
})

module.exports = router