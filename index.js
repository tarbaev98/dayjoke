const express = require('express')
const app = express()
const path = require('path')
const mongo = require('mongoose')

let hostname = '0.0.0.0'
let port = 3000

app.use(express.json())

app.use('/public', express.static(path.join(__dirname, 'public')))

app.use('/api', require('./routes/joke.routes'))

app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname,'index.html'))
})

async function start() {
    try {
        await mongo.connect("mongodb+srv://root:12345678Qq@cluster0.my55d.mongodb.net/joke",
            {
            useNewUrlParser: true,
            useFindAndModify: false
            }
        )

        app.listen(port, hostname , () => {
            console.log(`Сервер запущен по адресу http://${hostname}:${port}`)
        })
    } catch (e) {
        console.log(e)
    }

}

start()
