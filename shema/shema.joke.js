const {Schema, model} = require('mongoose')

const schema = new Schema({
    id: { type: String, required: true},
    joke: { type: String, required: true},
    like: {type: Number, default: 0}
})

module.exports = model('Jokes', schema)